default:
	echo probably should run make build

clean:
	rm -f zola

zola:
	curl -sL https://github.com/getzola/zola/releases/download/v0.6.0/zola-v0.6.0-x86_64-unknown-linux-gnu.tar.gz | tar zxv

build: zola
	./zola build
